let firebase = {
  apiKey: "AIzaSyBHmAKCRdpSMtP53TY7qXjoXPgCniG5T4c",
  authDomain: "hspc-tst.firebaseapp.com",
  databaseURL: "https://hspc-tst.firebaseio.com",
  projectId: "hspc-tst",
  storageBucket: "hspc-tst.appspot.com",
  messagingSenderId: "462717146607"
};

let app = {
  personaAuthEndpoint: "https://sandbox-test.logicahealth.org/REST/userPersona/authenticate",
  authActionFrameBaseUrl: "https://hspc-tst.firebaseapp.com/__/auth/action",
  accountApiUrl: "https://account-api-test.logicahealth.org" //"http://localhost:8066"
};

export default {firebase, app}
