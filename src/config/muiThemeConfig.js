import {
  cyan500,
  grey300,
  white,
  darkBlack,
  fullBlack,
} from 'material-ui/styles/colors';
import {fade} from 'material-ui/utils/colorManipulator';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

// https://material.io/color/#!/?view.left=1&view.right=1&primary.color=005778&secondary.color=AAE4FA
const muiTheme = getMuiTheme({
  palette: {
    primary1Color: '#005778',
    primary2Color: '#4584a7',
    primary3Color: '#002e4c',
    accent1Color: '#aae4fa',
    accent2Color: '#ddffff',
    accent3Color: '#78b2c7',
    textColor: '#005778',
    errorColor: '#F44336',
    alternateTextColor: '#ffffff',
    canvasColor: white,
    borderColor: grey300,
    disabledColor: fade(darkBlack, 0.3),
    pickerHeaderColor: cyan500,
    clockCircleColor: fade(darkBlack, 0.07),
    shadowColor: fullBlack,
  },
});

export default muiTheme;
