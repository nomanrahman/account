import React from "react";
import {RaisedButton, TextField, Checkbox, FontIcon} from "material-ui";
import PropTypes from "prop-types";
import EmailPassword from "./EmailPassword";
import muiThemeable from 'material-ui/styles/muiThemeable';

const CreateAccountForm = ({displayName, email, password, onCreateAccountWithGoogle, onChange, authenticating, errors, onCreateAccount, muiTheme, newsletter1, newsletter2}) => {

  const checkboxStyle = {fontSize: '12px', marginLeft: '-10px'};

  return (
    <form>
      {errors.create &&
      <p style={{color: muiTheme.palette.errorColor, fontSize: "80%"}}>{errors.create}</p>}
      <RaisedButton
        onClick={onCreateAccountWithGoogle}
        label="Create Account With Google"
        fullWidth={true}
        primary={true}
        disabled={authenticating}
        icon={<FontIcon className="fa fa-google"/>}
      />
      <br/>
      <div className="sign-in-divider">
        <hr/>
        <p>OR</p>
      </div>
      <TextField
        floatingLabelText="Full Name"
        value={displayName}
        onChange={onChange}
        name="displayName"
        errorText={errors.displayName}
        fullWidth={true}
      />
      <EmailPassword
        email={email}
        password={password}
        onChange={onChange}
        errors={errors}
      />
      <br/>
      <RaisedButton
        label={authenticating ? 'Creating Account...' : 'Create Account'}
        onClick={onCreateAccount}
        disabled={authenticating}
        fullWidth={true}
        primary={true}
      />
      <br/>
      <br/>
      <Checkbox
        className="newsletter-checkbox"
        label="Keep me up to date on HSPC Developers, Sandbox, and Gallery."
        checked={newsletter1}
        onCheck={onChange}
        labelStyle={checkboxStyle}
        name='newsletter1'
      />
      <Checkbox
        className="newsletter-checkbox"
        label="Join the HSPC mailing list."
        checked={newsletter2}
        onCheck={onChange}
        labelStyle={checkboxStyle}
        name='newsletter2'
      />
    </form>
  );
};

CreateAccountForm.propTypes = {
  displayName: PropTypes.string,
  email: PropTypes.string,
  password: PropTypes.string,
  onCreateAccount: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  authenticating: PropTypes.bool.isRequired,
  errors: PropTypes.object
};

export default muiThemeable()(CreateAccountForm);
