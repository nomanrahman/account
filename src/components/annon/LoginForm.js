import React from "react";
import {RaisedButton, FlatButton, FontIcon} from "material-ui";
import PropTypes from "prop-types";
import EmailPassword from "./EmailPassword";
import {Link} from "react-router-dom";
import muiThemeable from 'material-ui/styles/muiThemeable';

const LoginForm = ({email, password, onLogIn, onChange, authenticating, errors, muiTheme, onLogInWithGoogle}) => {
  return (
    <form>
      <RaisedButton
        onClick={onLogInWithGoogle}
        label="LogIn With Google"
        fullWidth={true}
        primary={true}
        disabled={authenticating}
        icon={<FontIcon className="fa fa-google"/>}
      />
      {errors.google &&
      <p style={{color: muiTheme.palette.errorColor, fontSize: "80%"}}>{errors.google}</p>}
      <br/>
      <div className="sign-in-divider">
        <hr/>
        <p>OR</p>
      </div>
      <EmailPassword
        email={email}
        password={password}
        onChange={onChange}
        errors={errors}
        authenticating={authenticating}
      />
      {errors.password &&
      <p style={{color: muiTheme.palette.textColor, fontSize: "80%"}}>If this is your first time using the new HSPC account system, you
        need to <Link to="/annon/reset-password/first-time">reset your password</Link>.</p>}
      {errors.login &&
      <p style={{color: muiTheme.palette.errorColor, fontSize: "80%"}}>{errors.login}</p>}
      <br/>
      <RaisedButton
        label={authenticating ? 'Authenticating...' : 'LogIn'}
        onClick={onLogIn}
        disabled={authenticating}
        fullWidth={true}
        primary={true}
      />
      <br/>
      <FlatButton labelStyle={{fontSize: '70%'}} label="Forgot password?" containerElement={<Link to="/annon/reset-password"/>}/>

    </form>
  );
};

LoginForm.propTypes = {
  email: PropTypes.string,
  password: PropTypes.string,
  onLogIn: PropTypes.func.isRequired,
  onLogInWithGoogle: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  authenticating: PropTypes.bool.isRequired,
  errors: PropTypes.object
};

export default muiThemeable()(LoginForm);
