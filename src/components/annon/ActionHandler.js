import React from "react";
import { RaisedButton, CircularProgress } from "material-ui";
import PropTypes from "prop-types";
import muiThemeable from "material-ui/styles/muiThemeable";
import config from '../../config/config';
import * as Firebase from "firebase";

class ActionHandler extends React.Component {

    constructor (props) {
        super(props);

        this.state = {
            iFrameSrc: "",
            emailVerified: false,
            verificationFinished: false
        }
    }

    componentWillMount () {

        let query = document.location.search;

        this.setState({
            iFrameSrc: config.app.authActionFrameBaseUrl + query
        });
    }

    componentDidMount () {
        let urlParams = new URLSearchParams(window.location.search);
        let code = urlParams.get('oobCode');
        code && Firebase.auth().applyActionCode(code)
            .then(() => {
                this.setState({ verificationFinished: true, emailVerified: true })
            })
            .catch(error => {
                console.log(error);
                this.setState({ verificationFinished: true, errorMessage: error.message ? error.message : null });
            });
    }

    render () {
        let message = this.state.emailVerified
            ? <span className="verification-success">Email verified!</span>
            : <span className="verification-error">{this.state.errorMessage
                ? this.state.errorMessage
                : "There was a problem with the email verification. Please try again later or contact our support."}</span>;

        let redirectLink = window.location.href.indexOf("-test.") >= 0
            ? "https://sandbox-test.logicahealth.org"
            : "https://sandbox.logicahealth.org";

        return <div>
            <div>
                {!this.state.verificationFinished &&
                <span className="verification-success">
                    <CircularProgress size={60} thickness={7} />
                    <div>Verifying email address</div>
                </span>}
                {this.state.verificationFinished && message}
            </div>
            <a href={redirectLink}>
                <RaisedButton primary={true} fullWidth={true} label="Go to sandbox manager" />
            </a>
        </div>;
    }
}

ActionHandler.propTypes = {
    firebase: PropTypes.object.isRequired
};

export default muiThemeable()(ActionHandler);
