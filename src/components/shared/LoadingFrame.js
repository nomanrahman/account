import React from "react";
import "./LoadingFrame.css";

function LoadingFrame() {
  return (
    <div className="slim-page-header">Loading...</div>
  );
}

LoadingFrame.propTypes = {};
LoadingFrame.defaultProps = {};

export default LoadingFrame;
