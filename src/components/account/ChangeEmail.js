import React from 'react';
import {TextField} from "material-ui";
import muiThemeable from 'material-ui/styles/muiThemeable';

const ChangeEmail = ({password, newEmail, onChange, errors, updateErrors, updating, muiTheme}) => {
  return (
    <div>
      <TextField
        value={password}
        floatingLabelText="Current Password"
        type="password"
        onChange={onChange}
        name="curPassword"
        errorText={errors.password}
        fullWidth={true}
        disabled={updating}
        autoComplete="new-password"
      />
      <br/>
      <TextField
        value={newEmail}
        floatingLabelText="New Email"
        onChange={onChange}
        name="newEmail"
        errorText={errors.message}
        fullWidth={true}
      />
      {updateErrors.message &&
      <p style={{color: muiTheme.palette.errorColor, fontSize: "80%"}}>{updateErrors.message}</p>}
    </div>
  );
};

export default muiThemeable()(ChangeEmail);
