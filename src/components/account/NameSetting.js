import React from 'react';
import {FlatButton, Dialog, IconButton} from "material-ui";
import EditorModeEdit from 'material-ui/svg-icons/editor/mode-edit';
import PropTypes from 'prop-types';
import ChangeName from "./ChangeName";

class NameSetting  extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      open: false
    };
  }

  handleOpen = () => {
    this.setState({open: true});
  };

  handleClose = () => {
    this.setState({open: false});
    this.props.onCancel();
  };

  handleSubmit = () => {
    this.props.onUpdateUsername();
    this.setState({open: false});
  };

  render() {
    const actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onClick={this.handleClose}
      />,
      <FlatButton
        label="Submit"
        primary={true}
        disabled={this.props.newDisplayName && this.props.newDisplayName.length === 0}
        onClick={this.handleSubmit}
      />,
    ];

    return (
      <div className="account-settings-box" style={{display: 'flex'}}>
        <div className="account-settings-label">Name</div>
        <div style={{flexGrow: 1}}>{this.props.currentUser.displayName}</div>
        <IconButton
          style={{width: 'auto'}}
          disabled={this.props.updating}
        >
          <EditorModeEdit onClick={this.handleOpen}/>
        </IconButton>
        <Dialog
          title="Change Name"
          contentClassName="account-settings-dialog"
          contentStyle={{width: "400px", maxWidth: "400px"}}
          actions={actions}
          modal={true}
          open={this.state.open || !this.props.success}
        >
          <ChangeName
            newName={this.props.newDisplayName}
            onChange={this.props.onChange}
            errors={this.props.errors} />
        </Dialog>
      </div>
    );
  }
}

NameSetting.PropTypes = {
  currentUser: PropTypes.object,
  newDisplayName: PropTypes.string,
  updating: PropTypes.bool,
  success: PropTypes.bool,
  onChange: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
  onUpdateUsername: PropTypes.func.isRequired,
  errors: PropTypes.object
};

export default NameSetting;
