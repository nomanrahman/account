import React from 'react';
import {TextField} from "material-ui";
import NewPassword from './NewPassword';
import muiThemeable from 'material-ui/styles/muiThemeable';

const ChangePassword = ({curPassword, newPassword, onChange, onValidatePassword, errors, updating, muiTheme}) => {
  return (
    <div>
      <TextField
        value={curPassword}
        floatingLabelText="Current Password"
        type="password"
        onChange={onChange}
        name="curPassword"
        errorText={errors.password}
        fullWidth={true}
        disabled={updating}
        autoComplete="new-password"
      />
      <br/>
      <NewPassword
        newPassword={newPassword}
        onValidatePassword={onValidatePassword}
        onChange={onChange}
        updating={updating}
      />
      {errors.message &&
      <p style={{color: muiTheme.palette.errorColor, fontSize: "80%"}}>{errors.message}</p>}
    </div>
  );
};

export default muiThemeable()(ChangePassword);
