import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import HspcAccountManager from "./components/HspcAccountManager";

ReactDOM.render(
  <HspcAccountManager/>,
  document.getElementById('root')
);
